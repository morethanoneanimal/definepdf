package com;

import com.vaadin.event.dd.DropHandler;
import com.vaadin.ui.Component;
import com.vaadin.ui.DragAndDropWrapper;

/**
 * Wrapper służący do opakowania komponentu tak, aby mógł być wykorzystany do drag'n'drop.
 * Kod wzorowany na przykładowym kodzie z dokumentacji Vaadin 7.
 * @author Marcin Salega
 * @author Mateusz Danek
 */
public class WrappedComponent extends DragAndDropWrapper {

    private final DropHandler dropHandler;


    public WrappedComponent(Component content, final DropHandler dropHandler) {
        super(content);
        this.dropHandler = dropHandler;
        setDragStartMode(DragStartMode.WRAPPER);
    }

    @Override
    public DropHandler getDropHandler() {
        return dropHandler;
    }

}