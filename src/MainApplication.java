package com;

import java.io.File;
import java.util.*;

import com.vaadin.data.Item;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.event.ItemClickEvent; //import com.vaadin.terminal.ThemeResource;
//import com.vaadin.terminal.gwt.client.ui.dd.HorizontalDropLocation;
import com.vaadin.server.*;
import com.vaadin.ui.*;

/** Podstawowa klasa aplikacji, implementacja klasy dostarczanej przez Vaadin.
 * @author Marcin Salega
 * @author Mateusz Danek
*/
public class MainApplication extends UI {

	private VerticalLayout mainLayout = new VerticalLayout();
	private VerticalLayout actionLayout = new VerticalLayout(); // I'm really
																// bad when it
																// comes to name
																// things
	private HorizontalLayout menuLayout = new HorizontalLayout();
	private VerticalLayout modifyLayout = new VerticalLayout();

	private static Button btnGenerate = new Button("GENERATE!");
	private static Button btnModify = new Button("Modify");
	private static Button btnDelete = new Button("Delete");

	static ComboBox selectFont = new ComboBox("Font");
	static ComboBox selectColor = new ComboBox("Color");
	static ComboBox selectSize = new ComboBox("Size");

	private static TextField txtContent = new TextField("Text:");
	private static Object selectedObject = null;

	public MainApplication() {
	}

    /**
     * Metoda inicjalizująca, inicjalizacja komponentów, tworzenie listenerów, rozwiązywanie zależności.
     * @param request
     */
	@Override
	public void init(VaadinRequest request) {

		final Tree tree = new Tree("PDF Structure");
		tree.setWidth("400px");
		// Populate the tree
		HierarchicalContainer container = GenerateContainer();

		tree.setContainerDataSource(container);
		tree.setItemCaptionPropertyId("caption");

		// Expand all nodes
		for (Iterator<?> it = tree.rootItemIds().iterator(); it.hasNext();) {
			tree.expandItemsRecursively(it.next());
		}
		tree.setDragMode(Tree.TreeDragMode.NODE);
		TreeDropHandler tdh = new TreeDropHandler(tree, container);
		tree.setDropHandler(tdh);
		tree.addItemClickListener(new TreeItemClickListener());

		btnModify.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(Button.ClickEvent clickEvent) {
				if (selectedObject != null) {
					Item it = tree.getItem(selectedObject);
					it.getItemProperty("content").setValue(
							txtContent.getValue());

					if (selectColor.getValue() != null)
						it.getItemProperty("color").setValue(
								selectColor.getValue().toString());
					if (selectSize.getValue() != null)
						it.getItemProperty("size").setValue(
								(selectSize.getValue().toString()));
					if (selectFont.getValue() != null)
						it.getItemProperty("font").setValue(
								(selectFont.getValue().toString()));
				} else
					Notification.show("select something from the tree");
			}
		});

		btnDelete.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(Button.ClickEvent clickEvent) {
				if (selectedObject != null) {
					tree.getContainerDataSource().removeItem(selectedObject);

					btnModify.setEnabled(false);
					txtContent.setEnabled(false);
					selectFont.setEnabled(false);
					selectColor.setEnabled(false);
					selectSize.setEnabled(false);

				} else
					Notification.show("select something from the tree");
			}
		});

		selectFont.addItem("sans-serif");
		selectFont.addItem("verdana");
		selectFont.addItem("arial");

		selectColor.addItem("red");
		selectColor.addItem("blue");
		selectColor.addItem("green");
		selectColor.addItem("yellow");

		selectSize.addItem("8");
		selectSize.addItem("12");
		selectSize.addItem("16");
		selectSize.addItem("20");
		selectSize.addItem("28");
		selectSize.addItem("34");

		selectFont.setNullSelectionAllowed(false);
		selectColor.setNullSelectionAllowed(false);
		selectSize.setNullSelectionAllowed(false);
		selectFont.setEnabled(false);
		selectColor.setEnabled(false);
		selectSize.setEnabled(false);
		// generowanie menu
		// TODO: do osobnej metody
		menuLayout.setMargin(true);
		menuLayout.addComponent(new com.WrappedComponent(new ComponentWithData(
				"Table", "Table", true), tdh));
		menuLayout.addComponent(new com.WrappedComponent(new ComponentWithData(
				"Label", "Label", false), tdh));
		menuLayout.addComponent(new com.WrappedComponent(new ComponentWithData(
				"Table header", "TableHeader", true), tdh));
		menuLayout.addComponent(new com.WrappedComponent(new ComponentWithData(
				"Table cell", "TableCell", true), tdh));
		menuLayout.addComponent(new com.WrappedComponent(new ComponentWithData(
				"Table row", "TableRow", true), tdh));

		modifyLayout.setMargin(true);
		modifyLayout.addComponent(selectSize);
		modifyLayout.addComponent(selectFont);
		modifyLayout.addComponent(selectColor);
		modifyLayout.addComponent(txtContent);
		modifyLayout.addComponent(btnModify);
		modifyLayout.addComponent(btnDelete);

		actionLayout.setMargin(true);
		actionLayout.addComponent(menuLayout);

		HorizontalLayout middleLayout = new HorizontalLayout();

		middleLayout.addComponent(modifyLayout);
		middleLayout.addComponent(tree);

		actionLayout.addComponent(middleLayout);

		FileDownloader downloader = new FileDownloader(new StreamResource(
				new com.Result(tree), "schema.xsl"));
		downloader.extend(btnGenerate);

		mainLayout.setMargin(true);
		// Find the application directory
		String basepath = VaadinService.getCurrent().getBaseDirectory()
				.getAbsolutePath();

		// Image as a file resource
		FileResource resource = new FileResource(new File(basepath
				+ "/WEB-INF/images/pdf_logo.png"));

		// Show the image in the application
		Image image = new Image("", resource);

		mainLayout.addComponent(image);
		mainLayout.addComponent(actionLayout);
		btnGenerate.setWidth("200px");
		btnModify.setEnabled(false);
		txtContent.setEnabled(false);
		mainLayout.addComponent(btnGenerate);
		mainLayout.setComponentAlignment(btnGenerate, Alignment.BOTTOM_CENTER);

		setContent(mainLayout);
	}

    /**
     * Utworzenia obiektu typu HierarchicalContainer przygotowanego do przechowywania struktury.
     * @return
     */

	private HierarchicalContainer GenerateContainer() {
		HierarchicalContainer container = new HierarchicalContainer();
		container.addContainerProperty("caption", String.class, " x ");
		container.addContainerProperty("content", String.class, " x ");
		container.addContainerProperty("font", String.class, " x ");
		container.addContainerProperty("color", String.class, " x ");
		container.addContainerProperty("size", String.class, " x ");
		Item i = container.addItem(0);
		i.getItemProperty("caption").setValue("root");
		container.setChildrenAllowed(0, true);
		Item ch = container.addItem(1);
		ch.getItemProperty("caption").setValue("Label");

		container.setParent(1, 0);

		return container;
	}

    /**
     * @deprecated     *
     * @param item
     * @return
     */

	private static String getDescription(Item item) {
		String caption = (String) item.getItemProperty("caption").getValue();
		if (caption.equals("Table"))
			return "Nothing you can do here";
		else if (caption.equals("Label"))
			return "Enter the text that should be displayed.";
		return "no description";
	}

    /**
     * Klasa implementująca zachowanie drzewa po kliknięciu.
     * Odbywa się tu uaktualnienie atrybutów.
     */
	private static class TreeItemClickListener implements
			ItemClickEvent.ItemClickListener {
		public void itemClick(ItemClickEvent ev) {
			selectedObject = ev.getItemId();
			Item it = ev.getItem();
			txtContent.setValue((String) it.getItemProperty("content")
					.getValue());

			String caption = (String) it.getItemProperty("caption").getValue();
			if (caption.equals("Label")) {
				btnModify.setEnabled(true);
				txtContent.setEnabled(true);
				selectFont.setEnabled(true);
				selectColor.setEnabled(true);
				selectSize.setEnabled(true);
			} else {
				btnModify.setEnabled(false);
				txtContent.setEnabled(false);
				selectFont.setEnabled(false);
				selectColor.setEnabled(false);
				selectSize.setEnabled(false);
			}

		}
	}

}
