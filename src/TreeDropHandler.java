package com;

import com.WrappedComponent;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.event.DataBoundTransferable;
import com.vaadin.event.Transferable;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptAll;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.shared.ui.dd.VerticalDropLocation;
import com.vaadin.ui.DragAndDropWrapper;
import com.vaadin.ui.Tree;

/**
 * Implementacja zachowania kontrolki Tree po upuszczeniu na nią komponentu (wrappowanego w WrappedComponent).
 * @author Marcin Salega
 * @author Mateusz Danek
 * @see com.WrappedComponent
 */
public class TreeDropHandler implements DropHandler {
    private final Tree tree;

    /**
     *
     * @param tree Kontrolka, której zachowanie ma być implementowane.
     * @param container Zawartość kontrolki
     */
    public TreeDropHandler(Tree tree, HierarchicalContainer container) {
        this.tree = tree;
    }

    /**
     *
     * @return Kryterium określające jaki rodzaj obiektów może przyjąć kontrolka.
     */
    public AcceptCriterion getAcceptCriterion() {
        // Alternatively, could use the following criteria to eliminate some
        // checks in drop():
        // new And(IsDataBound.get(), new DragSourceIs(tree));
        return AcceptAll.get();
    }

    /**
     * Obługa eventu, jes tu kod odpowiedzialny za przesuwanie oraz dodawanie nowych elementów.
     * Ponadto weryfikowana jest pozycja dodanych/przesuwanych elementów tak, aby nie zostały umieszczone poza elementem Root.
     * @param dropEvent
     */
    public void drop(DragAndDropEvent dropEvent) {
        // Called whenever a drop occurs on the component

        // Make sure the drag source is the same tree
        Transferable t = dropEvent.getTransferable();

        Tree.TreeTargetDetails dropData = ((Tree.TreeTargetDetails) dropEvent.getTargetDetails());
        Object sourceItemId;
        if( t instanceof DragAndDropWrapper.WrapperTransferable) {
            com.ComponentWithData data = (ComponentWithData) ((DragAndDropWrapper.WrapperTransferable) t).getDraggedComponent();
            sourceItemId = tree.addItem();
            tree.getItem(sourceItemId).getItemProperty("caption").setValue(data.getElementName());
            tree.getItem(sourceItemId).getItemProperty("content").setValue("");
            tree.setChildrenAllowed(sourceItemId, data.getCanHaveChildren());
        }
        else {
            sourceItemId = ((DataBoundTransferable) t).getItemId();
        }
        // FIXME: Why "over", should be "targetItemId" or just
        // "getItemId"
        Object targetItemId = dropData.getItemIdOver();

        // Location describes on which part of the node the drop took
        // place
        VerticalDropLocation location = dropData.getDropLocation();
        if( targetItemId instanceof Integer && (Integer)targetItemId == 0 && location != VerticalDropLocation.MIDDLE) // zeby nie dalo sie przesunac/dodac czegos ponad rootem
            location = VerticalDropLocation.MIDDLE;
        moveNode(sourceItemId, targetItemId, location);
    }

    /**
     * Przesunięcie node'a w drzewie na, pod lub nad inny node, w zależności od location.
     *
     * @param sourceItemId
     *            id node'a do przesunięcia

     * @param targetItemId
                 id node'a, na który source item jest przesuwany
     * @param location
     *            Pozycja, na której ma być przesunięty source item (względem target item'u)
     */
    private void moveNode(Object sourceItemId, Object targetItemId,
                          VerticalDropLocation location) {
        HierarchicalContainer container = (HierarchicalContainer) tree
                .getContainerDataSource();

        // Sorting goes as
        // - If dropped ON a node, we append it as a child
        // - If dropped on the TOP part of a node, we move/add it before
        // the node
        // - If dropped on the BOTTOM part of a node, we move/add it
        // after the node

        if (location == VerticalDropLocation.MIDDLE) {
            if (container.setParent(sourceItemId, targetItemId)
                    && container.hasChildren(targetItemId)) {
                // move first in the container
                container.moveAfterSibling(sourceItemId, null);
            }
        } else if (location == VerticalDropLocation.TOP) {
            Object parentId = container.getParent(targetItemId);
            if (container.setParent(sourceItemId, parentId)) {
                // reorder only the two items, moving source above target
                container.moveAfterSibling(sourceItemId, targetItemId);
                container.moveAfterSibling(targetItemId, sourceItemId);
            }
        } else if (location == VerticalDropLocation.BOTTOM) {
            Object parentId = container.getParent(targetItemId);
            if (container.setParent(sourceItemId, parentId)) {
                container.moveAfterSibling(sourceItemId, targetItemId);
            }
        }
    }
}
