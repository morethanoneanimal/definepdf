package com;
import com.vaadin.ui.Button;

/**
 * Komponent reprezentujący element w pliku xml. Może zostać przeciągnięty na kontrolkę typu Tree.
 * Przedstawia elementy typu: Label, TableHeader, TableRow itp.
 * @author Marcin Salega
 * @author Mateusz Danek
 */
public class ComponentWithData extends Button {
    /**
     *
     * @param buttonCaption
     *          napis na buttonie
     * @param elementName
     *          nazwa elementu, pod tą nawzą widoczny będzie element na kontrolce typu Tree
     * @param canHaveChildren
     *          czy reprezentowany element może mieć zawierać inne elementy
     */
    public ComponentWithData(String buttonCaption, String elementName, boolean canHaveChildren) {
        super(buttonCaption);
        this.setWidth("140px");
        this.elementName = elementName;
        this.canHaveChildren = canHaveChildren;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public boolean getCanHaveChildren() {
        return canHaveChildren;
    }

    public void setCanHaveChildren(boolean canHaveChildren) {
        this.canHaveChildren = canHaveChildren;
    }

    private String elementName;
    private boolean canHaveChildren;
}
