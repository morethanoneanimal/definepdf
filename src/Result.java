package com;

import com.vaadin.data.Item;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.server.DownloadStream;
import com.vaadin.server.StreamResource;
import com.vaadin.ui.Tree;
import org.apache.geronimo.mail.util.StringBufferOutputStream;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.management.Notification;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;


/**
 * Zadaniem tej klasy jest wygenerowanie pliku xml, który jest zwracany w odpowiedzi na żądanie klienta. Należy podać jej kontrolkę, na podstawie której ma utworzyć plik.
 * @author Marcin Salega
 * @author Mateusz Danek
 */
public class Result implements StreamResource.StreamSource {
    Tree tree;

    /**
     *
     * @param t Kontrolka, na podstawie której tworzony jest plik xml.
     */
    public Result(Tree t) {
        super();
        setTree(t);
    }

    public Result() {
    }

    /**
     *
     * @return Kontrolka, na podstawie której tworzony jest plik xml.
     */
    public Tree getTree() {
        return tree;
    }

    /**
     *
     * @param tree Kontrolka, na podstawie której tworzony jest plik xml.
     */
    public void setTree(Tree tree) {
        this.tree = tree;
    }

    private Document doc = null;
    @Override
    /**
     * Generowanie pliku xml oraz przedstawienie go w postaci strumienia bajtów..
     */
    public InputStream getStream() {
        HierarchicalContainer c = (HierarchicalContainer) tree.getContainerDataSource();
        if(c.rootItemIds().size() > 1) {
            com.vaadin.ui.Notification.show("Error in structure: too many roots");
            return null;
        }
        Object rootId = c.rootItemIds().toArray()[0];
        Object[] children = c.getChildren(rootId).toArray();

        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("xsl:stylesheet");
            rootElement.setAttribute("xmlns:xsl", "http://www.w3.org/1999/XSL/Transform");
            rootElement.setAttribute("xmlns:fo", "http://www.w3.org/1999/XSL/Format");
            rootElement.setAttribute("version", "1.0");
            doc.appendChild(rootElement);
            Element xslOutputElement = doc.createElement("xsl:output");
            xslOutputElement.setAttribute("method", "xml");
            xslOutputElement.setAttribute("indent", "yes");
            
            Element template = doc.createElement("xsl:template");
        	template.setAttribute("match", "raport");
            
            rootElement.appendChild(xslOutputElement);
            Element foRootElement = GenerateHeader();
            
            template.appendChild(foRootElement);
            
            Element pageSeqElement = GeneratePageSequence();
            
            Element flowElement = doc.createElement("fo:flow");
            flowElement.setAttribute("flow-name", "xsl-region-body");
            
            pageSeqElement.appendChild(flowElement);
            
            foRootElement.appendChild(pageSeqElement);
            rootElement.appendChild( template);

            // more work
            GenerateContent(children, flowElement, c);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(doc);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            StreamResult result = new StreamResult(outputStream);
            transformer.transform(domSource, result);
            return new ByteArrayInputStream(outputStream.toByteArray());
        }
        catch(Exception e) {
        	e.printStackTrace();
            com.vaadin.ui.Notification.show("Exception while creating xsl: " + e.getClass().toString());
            return null;
        }
    }

    /**
     * Rekurencyjne generowanie xml'a na podstawie struktury.
     * @param childrenIds
     *      Zbiór id potomków głównego elementu
     * @param mainElement
     *      Główny element
     * @param container
     *      Struktura, na podstawie której tworzony jest xml.
     * @return
     *      Pełen element z wygenerowaną zawartością (również z potomkami).
     */

    private Element GenerateContent(Object[] childrenIds, Element mainElement, HierarchicalContainer container) {
        if(childrenIds == null || childrenIds.length == 0)
            return null;
        for(Object childId: childrenIds) {
            Item item = container.getItem(childId);
            Object[] grandchildrenIds = null;
            if(container.getChildren(childId) != null)
                grandchildrenIds = container.getChildren(childId).toArray();
            Element newElement = GenerateElement(item);
            if( newElement.getNodeName().equals("fo:table")) {
            	Element tableBody = doc.createElement("fo:table-body");
            	newElement.appendChild(tableBody);
            	GenerateContent(grandchildrenIds, tableBody, container);
            }
            else
            
            GenerateContent(grandchildrenIds, newElement, container);
            mainElement.appendChild(newElement);
            
        }
        return null; // TODO: rethink that
    }

    /**
     * Wygenerowanie kodu xml na podstawie rodzaju elementu.
     * @param item
     * @return
     */
    private Element GenerateElement(Item item) {
        // wyciaganie properties
        String caption = (String) item.getItemProperty("caption").getValue();
        String content = (String) item.getItemProperty("content").getValue();
        Element el = null;
        if(caption.equals("TableRow"))
            el = doc.createElement("fo:table-row");
        else if(caption.equals("Table")) {
            el = doc.createElement("fo:table");
            
        }
        else if(caption.equals("TableCell"))
            el = doc.createElement("fo:table-cell");
        else if(caption.equals("TableHeader")) 
            el = doc.createElement("fo:table-header");
            
           
        
        else {
            el = doc.createElement("fo:block");
        	
        	 String color = (String) item.getItemProperty("color").getValue();
             String size = (String) item.getItemProperty("size").getValue();
             String font = (String) item.getItemProperty("font").getValue();
             
             if( color != null )
             	el.setAttribute("color", color);
             if( font != null )
             	el.setAttribute("font-family", font);
             if( size != null )
             	el.setAttribute("font-size", size);
     
        while(true) {
        	int start = content.indexOf("${");
        	if( start == -1)
        		break;
        	int end = content.indexOf("}", start+1);
        	String var = content.substring(start+2, end);
        	System.out.println(var);
        	content = content.replace("${"+var+"}", "#bxsl:value-of select=\""+var+"\"/#e");
        }
        
        }
        
        el.setTextContent(content);
        return el;
    }

    /**
     * Generowanie sekwencji strony (charakterystyczny element).
     * @return
     */
    private Element GeneratePageSequence() {
        Element e = doc.createElement("fo:page-sequence");
        e.setAttribute("master-reference", "A4-portrait");
        
        
        return e;
    }

    /**
     * Generowanie nagłówka.
     * @return
     */
    private Element GenerateHeader() {
    	
    	
        Element root = doc.createElement("fo:root");
        Element layoutMasterSet = doc.createElement("fo:layout-master-set");
        Element simplePageMaster = doc.createElement("fo:simple-page-master");
        simplePageMaster.setAttribute("master-name", "A4-portrait");
        simplePageMaster.setAttribute("page-height", "29.7cm");
        simplePageMaster.setAttribute("page-width", "21.0cm");
        simplePageMaster.setAttribute("margin", "2.0cm");
        simplePageMaster.appendChild( doc.createElement("fo:region-body"));

        layoutMasterSet.appendChild(simplePageMaster);
        root.appendChild(layoutMasterSet);
        

        return root;
    }



}
