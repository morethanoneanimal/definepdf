package app;

import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

/** Class which uses .xml file containing raport variables and .xsl containing PDF definition to generate PDF file
 *
 * @author Marcin Salega
 * @author Mateusz Danek
 */
public class PdfGenerator {

    /** Method which changes .xsl file content to make it readable by Apache FOP mechanism
     *
     * @param xslFilePath   path to uploaded .xsl file
     *
     */
    private void processXslFile(String xslFilePath) {
        BufferedReader br = null;
        String content = "";
        try {

            br = new BufferedReader(new FileReader(xslFilePath));
            content = br.readLine();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        content = content.replaceAll("#b", "<");
        content = content.replaceAll("#e", ">");
        System.out.println(content);

        try {

            File file = new File(xslFilePath);

            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(content);
            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /** Method which generates PDF file
     *
     * @param xslFilePath   path to uploaded .xsl file
     *
     */
    public void generatePdf(String xslFilePath) throws Exception {


        // Setup input and output files
        File xmlfile = new File("var.xml");
        processXslFile(xslFilePath);
        File xsltfile = new File(xslFilePath);
        File pdffile = new File("output.pdf");

        final FopFactory fopFactory = FopFactory.newInstance();

        FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
        OutputStream out = new java.io.FileOutputStream(pdffile);
        out = new java.io.BufferedOutputStream(out);

        Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);


        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer(new StreamSource(xsltfile));
        transformer.setParameter("versionParam", "2.0");
        Source src = new StreamSource(xmlfile);
        Result res = new SAXResult(fop.getDefaultHandler());
        transformer.transform(src, res);
        if (out != null)
            out.close();


    }

}
