package app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/** Frame which allows user to select and upload .xsl file containing PDF definition
 *
 * @author Marcin Salega
 * @author Mateusz Danek
 */
public class NextFrame extends JFrame {

    /** Path to .xsl file containing PDF definition */
    private String xslFilePath = "";

    /** Constructor of the frame which builds and displays it
     *
     * @param parent   reference to main frame
     *
     */
    public NextFrame(final PdfCreatorFrame parent) {
        super("PDF Creator");
        parent.setVisible(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(400, 150);
        setResizable(false);
        JPanel outerPanel = new JPanel();

        JPanel uploadPanel = new JPanel();
        GridLayout uploadLayout = new GridLayout(1,2, 30, 30);
        uploadPanel.setLayout(uploadLayout);
        JLabel uploadLabel = new JLabel("Select a .xsl file");
        uploadLabel.setHorizontalAlignment(SwingConstants.CENTER);
        JButton browseButton = new JButton("Browse");
        browseButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                JFileChooser fileChooser = new JFileChooser();
                int returnValue = fileChooser.showOpenDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    xslFilePath = fileChooser.getSelectedFile().getAbsolutePath();
                }
            }
        });
        uploadPanel.add(uploadLabel);
        uploadPanel.add(browseButton);

        JPanel buttonsPanel = new JPanel();
        GridLayout buttonsLayout = new GridLayout(1,2, 30, 30);
        buttonsPanel.setLayout(buttonsLayout);
        JButton backButton = new JButton("Back");
        buttonsPanel.add(backButton);
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                NextFrame.this.setVisible(false);
                parent.setVisible(true);
            }
        });
        JButton generateButton = new JButton("Generate PDF");
        generateButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                if( !xslFilePath.equals("") ){
                    try {
                        new XmlGenerator().generateXml(parent.name, parent.surname, parent.city, parent.street,
                                parent.homeNumber, parent.postalCode, parent.productTable);
                        new PdfGenerator().generatePdf(xslFilePath);

                        JOptionPane.showMessageDialog(new JFrame(), "PDF file has been generated.", "PDF Creator",
                                JOptionPane.INFORMATION_MESSAGE);
                        NextFrame.this.setVisible(false);
                        parent.setVisible(true);
                    }
                    catch(Exception e)
                    {
                        JOptionPane.showMessageDialog(new JFrame(), "Generating PDF file failed.", "Error",
                                JOptionPane.ERROR_MESSAGE);
                        e.printStackTrace();
                        NextFrame.this.setVisible(false);
                        parent.setVisible(true);
                    }
                } else {
                    JOptionPane.showMessageDialog(new JFrame(), "Please select a .xsl file.", "PDF Creator",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        buttonsPanel.add(generateButton);

        uploadPanel.setBackground(new Color(255, 252, 141));
        buttonsPanel.setBackground(new Color(255, 252, 141));
        outerPanel.setBackground(new Color(255, 252, 141));
        outerPanel.add(uploadPanel);
        outerPanel.add(buttonsPanel);
        getContentPane().add(outerPanel);
        getContentPane().setBackground(new Color(255, 252, 141));
        setLocationRelativeTo(null);
        setVisible(true);
    }
}
