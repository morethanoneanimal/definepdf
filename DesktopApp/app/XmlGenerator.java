package app;

import javax.swing.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/** Class which generates var.xml file containing user-defined variables which can be used in a report
 *
 * @author Marcin Salega
 * @author Mateusz Danek
 */
public class XmlGenerator {

    /** Method which generates var.xml file content and invokes file creating method
     *
     * @param name          user name
     * @param surname       user surname
     * @param city          user city
     * @param street        user street
     * @param homeNumber    user home number
     * @param postalCode    user postal code
     * @param products      list of products
     * @return			    var.xml file content
     *
     */
    public void generateXml(String name, String surname, String city,
                            String street, String homeNumber, String postalCode, JTable products) {
        String xmlOutput = "<raport>\n<name>"+name+"</name>\n"+"<surname>"+surname+"</surname>\n"+"<city>"+city+"</city>\n"+
                "<street>"+street+"</street>\n"+"<homeNumber>"+homeNumber+"</homeNumber>\n"+"<postalCode>"+postalCode+"</postalCode>\n";


        for (int row = 0; row < products.getRowCount(); row++) {
            String productName = (String) products.getValueAt(row, 0);
            String price = (String) products.getValueAt(row, 1);
            String quantity = (String) products.getValueAt(row, 2);


            xmlOutput += "\t\t<productName"+(row+1)+">"+productName+"</productName"+(row+1)+">\n";
            xmlOutput += "\t\t<price"+(row+1)+">"+price+"</price"+(row+1)+">\n";
            xmlOutput += "\t\t<quantity"+(row+1)+">"+quantity+"</quantity"+(row+1)+">\n";
        }

        xmlOutput += "</raport>";


        createFile(xmlOutput);
    }

    /** Method which generates var.xml file using given content
     *
     * @param content   var.xml file content
     */
    private void createFile(String content) {
        try {

            File file = new File("var.xml");

            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(content);
            bw.close();
            fw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
