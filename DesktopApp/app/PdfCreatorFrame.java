package app;

import javax.swing.ImageIcon;import javax.swing.JButton;import javax.swing.JFrame;import javax.swing.JLabel;import javax.swing.JPanel;import javax.swing.JScrollPane;import javax.swing.JTable;import javax.swing.JTextField;import javax.swing.SwingConstants;import javax.swing.WindowConstants;import javax.swing.table.DefaultTableModel;
import java.awt.Color;import java.awt.Dimension;import java.awt.GridLayout;import java.awt.Panel;import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;import java.lang.Double;import java.lang.Exception;import java.lang.Integer;import java.lang.String;

/** Main frame which allows user to define variables that can be used in a raport
 *
 * @author Marcin Salega
 * @author Mateusz Danek
 */
public class PdfCreatorFrame extends JFrame {

    /** user name */
    String name;
    /** user surname */
    String surname;
    /** user city */
    String city;
    /** user street */
    String street;
    /** user home number */
    String homeNumber;
    /** user postal code */
    String postalCode;
    /** products */
    JTable productTable;

    /** Constructor of the main frame  */
    public PdfCreatorFrame() {
        super("PDF Creator");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setSize(400, 670);
        setResizable(false);
        JPanel outerPanel = new JPanel();

        Panel logoPanel = new Panel();
        ImageIcon logoIcon = new ImageIcon(getClass().getClassLoader().getResource("resources/logo.png"));
        JLabel logoLabel = new JLabel(logoIcon);
        logoPanel.add(logoLabel);

        Panel formPanel = new Panel();
        formPanel.setPreferredSize(new Dimension(300, 150));
        GridLayout formLayout = new GridLayout(0, 2);
        JLabel nameLabel = new JLabel("Name: ");
        nameLabel.setHorizontalAlignment(SwingConstants.CENTER);
        JLabel surnameLabel = new JLabel("Surname: ");
        surnameLabel.setHorizontalAlignment(SwingConstants.CENTER);
        JLabel cityLabel = new JLabel("City: ");
        cityLabel.setHorizontalAlignment(SwingConstants.CENTER);
        JLabel streetLabel = new JLabel("Street: ");
        streetLabel.setHorizontalAlignment(SwingConstants.CENTER);
        JLabel homeNumberLabel = new JLabel("Home number: ");
        homeNumberLabel.setHorizontalAlignment(SwingConstants.CENTER);
        JLabel postalCodeLabel = new JLabel("Postal code: ");
        postalCodeLabel.setHorizontalAlignment(SwingConstants.CENTER);

        final JTextField nameTextField = new JTextField();
        final JTextField surnameTextField = new JTextField();
        final JTextField cityTextField = new JTextField();
        final JTextField streetTextField = new JTextField();
        final JTextField homeNumberTextField = new JTextField();
        final JTextField postalCodeTextField = new JTextField();

        formPanel.setLayout(formLayout);
        formPanel.add(nameLabel);
        formPanel.add(nameTextField);
        formPanel.add(surnameLabel);
        formPanel.add(surnameTextField);
        formPanel.add(cityLabel);
        formPanel.add(cityTextField);
        formPanel.add(streetLabel);
        formPanel.add(streetTextField);
        formPanel.add(homeNumberLabel);
        formPanel.add(homeNumberTextField);
        formPanel.add(postalCodeLabel);
        formPanel.add(postalCodeTextField);

        JPanel tablePanel = new JPanel();
        tablePanel.setBackground(new Color(255, 252, 141));
        final DefaultTableModel model = new DefaultTableModel();
        productTable = new JTable(model);
        JScrollPane tableScrollPane = new JScrollPane(productTable);
        tableScrollPane.setPreferredSize(new Dimension(350, 150));
        tablePanel.add(tableScrollPane);

        String[] columnNames = {"Product name",
                "Price per 1",
                "Quantity"};
        model.setColumnIdentifiers(columnNames);

        JPanel addProductPanel = new JPanel();
        GridLayout formLayout2 = new GridLayout(0, 2);
        JLabel productNameLabel = new JLabel("Product name: ");
        productNameLabel.setHorizontalAlignment(SwingConstants.CENTER);
        JLabel priceLabel = new JLabel("Price per 1: ");
        priceLabel.setHorizontalAlignment(SwingConstants.CENTER);
        JLabel quantityLabel = new JLabel("Quantity: ");
        quantityLabel.setHorizontalAlignment(SwingConstants.CENTER);
        final JTextField productNameTextField = new JTextField();
        final JTextField priceTextField = new JTextField();
        final JTextField quantityTextField = new JTextField();
        JButton addButton = new JButton("Add product");
        addButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (!productNameTextField.getText().equals("") && !priceTextField.getText().equals("")
                        && !quantityTextField.getText().equals("")) {

                    String[] newRow = new String[]{productNameTextField.getText(), priceTextField.getText(), quantityTextField.getText()};
                    model.addRow(newRow);
                }
            }
        });

        JButton removeButton = new JButton("Remove product");
        removeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (int row : productTable.getSelectedRows()) {
                    model.removeRow(row);
                }
            }
        });

        addProductPanel.setLayout(formLayout2);
        addProductPanel.add(addButton);
        addProductPanel.add(removeButton);
        addProductPanel.add(productNameLabel);
        addProductPanel.add(productNameTextField);
        addProductPanel.add(priceLabel);
        addProductPanel.add(priceTextField);
        addProductPanel.add(quantityLabel);
        addProductPanel.add(quantityTextField);

        addProductPanel.setBackground(new Color(255, 252, 141));

        JPanel nextPanel = new JPanel();
        nextPanel.setBackground(new Color(255, 252, 141));
        JButton nextButton = new JButton("Next");
        nextButton.setPreferredSize(new Dimension(300, 45));
        nextPanel.add(nextButton);
        nextButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if( isTableCorrect(productTable)) {
                    name = nameTextField.getText();
                    surname = surnameTextField.getText();
                    city = cityTextField.getText();
                    street = streetTextField.getText();
                    homeNumber = homeNumberTextField.getText();
                    postalCode = postalCodeTextField.getText();

                    new NextFrame(PdfCreatorFrame.this);
                }
            }
        });
        outerPanel.setBackground(new Color(255, 252, 141));
        outerPanel.add(logoPanel);
        outerPanel.add(formPanel);
        outerPanel.add(tablePanel);
        outerPanel.add(addProductPanel);
        outerPanel.add(nextPanel);
        getContentPane().add(outerPanel);
        getContentPane().setBackground(new Color(255, 252, 141));
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /** Method which validates products table content and allows or disallows to take the next step
     *
     * @param table   reference to products table
     *
     */
    private boolean isTableCorrect(JTable table) {

        if (table.getRowCount() == 0)
            return false;
        try {
            for (int row = 0; row < table.getRowCount(); row++) {
                Double price = Double.parseDouble((String) table.getValueAt(row, 1));
                Integer quantity = Integer.parseInt((String) table.getValueAt(row, 2));
                if (price < 0 || quantity < 1)
                    return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;

    }

    /** Main method
     *
     * @param args   command line arguments
     *
     */
    public static void main(String[] args) {
        new PdfCreatorFrame();
    }
}
